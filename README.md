# Daily Schedule

## _Description:_
        Daily Schedule, it is a website which provides platform to most common hierarchy found in companies like team member, team leader,  
    manager to organize their daily life schedule, tasks, information which will help them to improve their productivity, achieve goals  
    within time limit, setting priorities of tasks and keeping track of task progress, and can also share the schedule with the team members, 
    making team work more efficient and this will help people to keep track of time and utilize it in best possible way. This will also help  
    companies to view the progress of every employee at time of promotion. All these on one single platform.
## _Reference:_
>[Day Viewer](https://dayviewer.com "DayViewer")

![Daily Schedule Logo](https://cdn1.sph.harvard.edu/wp-content/uploads/sites/1266/2014/09/schedule-220x220.jpg)