const mysql = require("mysql"); // returns me object
const express = require("express"); // Express returns me function
const utils = require("./utils");

const employee = express();

const connection = mysql.createConnection({
    host: "localhost",
    user: "sunbeam",
    password: "sunbeam",
    database: "daily_schedule",
});
connection.connect();

var employeedata = [];
// employee.get("/", (req, res) => {
//     //Fire Query on DB
//     connection.query(`select * from employee`, (err, data) => {
//         res.send(utils.createResult(err, data));
//     });
// });

employee.get("/:employee_id", (req, res) => {
    connection.query(
        `select * from employee where employee_id = ${req.params.employee_id}`,
        (err, data) => {
            res.send(utils.createResult(err, data));
        }
    );
});

employee.get("/team-member/:team_id", (req, res) => {
    connection.query(
        `select * from employee where team_id = ${req.params.team_id}`,
        (err, data) => {
            res.send(utils.createResult(err, data));
        }
    );
});

employee.get("/manager/:manager_id", (req, res) => {
    connection.query(
        `select * from employee where manager_id = ${req.params.manager_id}`,
        (err, data) => {
            res.send(utils.createResult(err, data));
        }
    );
});

employee.get("/teamleader/:manager_id", (req, res) => {
    connection.query(
        `select * from employee where manager_id = ${req.params.manager_id} and role = 'Team Leader'`,
        (err, data) => {
            res.send(utils.createResult(err, data));
        }
    );
});

employee.post("/:manager_id", (req, res) => {
    //let employee_id  = req.body.employee_id;
    let manager_id = req.params.manager_id;
    let first_name = req.body.first_name;
    let last_name = req.body.last_name;
    let password = req.body.password;
    let role = req.body.role;
    let company_email = req.body.company_email;
    let company_mobile = req.body.company_mobile;
    let team_id = req.body.team_id;
    let birth_date = req.body.birth_date;
    // let personal_email = req.body.personal_email;
    // let personal_mobile = req.body.personal_mobile;
    // let address = req.body.address;
    let joining_date = req.body.joining_date;
    // let resign_date = req.body.resign_date;

    let query = `insert into employee(manager_id, first_name, last_name, password,role,company_email,
        company_mobile,team_id,birth_date,joining_date) values
        (${manager_id}, '${first_name}', '${last_name}', '${password}','${role}',
        '${company_email}','${company_mobile}',${team_id},'${birth_date}','${joining_date}')`;
    connection.query(query, (err, data) => {
        res.send(utils.createResult(err, data));
    });
});

// employee.put("/:employee_id", (req, res) => {
//     let manager_id = req.body.manager_id;
//     let first_name = req.body.first_name;
//     let last_name = req.body.last_name;
//     let password = req.body.password;
//     let team_id = req.body.team_id;
//     let personal_email = req.body.personal_email;
//     let personal_mobile = req.body.personal_mobile;
//     let address = req.body.address;
//     let query = `update employee set manager_id = ${manager_id}, first_name = '${first_name}', last_name = '${last_name}',
//         password = '${password}', team_id = ${team_id}, personal_email = '${personal_email}',
//         personal_mobile = '${personal_mobile}', address = '${address}' where employee_id = ${req.params.employee_id};`;
//     connection.query(query, (err, data) => {
//         res.send(utils.createResult(err, data));
//     });
// });

module.exports = employee;
