const mysql = require("mysql"); // returns me object
const express = require("express"); // Express returns me function
const utils = require("./utils");

const tasks_from_teamleader = express();

const connection = mysql.createConnection({
    host: "localhost",
    user: "sunbeam",
    password: "sunbeam",
    database: "daily_schedule",
});
connection.connect();

var tasks_from_teamleaderdata = [];
tasks_from_teamleader.get("/", (req, res) => {
    //Fire Query on DB
    connection.query("select * from tasks_from_teamleader", (err, data) => {
        res.send(utils.createResult(err, data));
    });
});

tasks_from_teamleader.get("/:manager_task_id", (req, res) => {
    connection.query(
        `select * from tasks_from_teamleader where manager_task_id = ${req.params.manager_task_id}`,
        (err, data) => {
            res.send(utils.createResult(err, data));
        }
    );
});

tasks_from_teamleader.get("/team-leader/:teamleader_id", (req, res) => {
    connection.query(
        `select * from tasks where assignee_id = ${req.params.teamleader_id}`,
        (err, data) => {
            res.send(utils.createResult(err, data));
        }
    );
});

tasks_from_teamleader.get("/team-member/:employee_id", (req, res) => {
    connection.query(
        `select * from tasks where assigned_id = ${req.params.employee_id}`,
        (err, data) => {
            res.send(utils.createResult(err, data));
        }
    );
});

tasks_from_teamleader.get("/task/:task_id", (req, res) => {
    connection.query(
        `select * from tasks where task_id = ${req.params.task_id}`,
        (err, data) => {
            res.send(utils.createResult(err, data));
        }
    );
});

tasks_from_teamleader.post("/:manager_task_id/:employee_id", (req, res) => {
    // let team_task_id  = req.params.team_task_id;
    // let manager_task_id  = req.params.manager_task_id;
    //let teamleader_id  = req.params.teamleader_id;
    //let team_id  = req.params.team_id;
    //let employee_id  = req.params.employee_id;
    //let title_id  = req.params.title_id;
    let task_title = req.body.task_title;
    let task_description = req.body.task_description;
    let start_date = req.body.start_date;
    let end_date = req.body.end_date;
    let status = req.body.status;

    let query = `insert into tasks_from_manager(manager_id,team_id,task_title,task_description,start_date, end_date,status) values
        ('${task_title}', '${task_description}','${start_date}','${end_date}', '${status}');`;
    connection.query(query, (err, data) => {
        res.send(utils.createResult(err, data));
    });
});

tasks_from_teamleader.put("/:team_task_id", (req, res) => {
    let employee_id = req.params.employee_id;
    let start_date = req.body.start_date;
    let end_date = req.body.end_date;
    let status = req.body.status;
    let query = `update tasks_from_teamleader set employee_id = '${employee_id}',start_date='${start_date}',end_date='${end_date}', status='${status}' where team_task_id = ${req.params.team_task_id};`;
    connection.query(query, (err, data) => {
        res.send(utils.createResult(err, data));
    });
});

tasks_from_teamleader.put("/task-update/:task_id", (req, res) => {
    let task_id = req.params.task_id;
    let status = req.body.status;
    let query = `update tasks set status='${status}' where task_id = ${task_id};`;
    connection.query(query, (err, data) => {
        res.send(utils.createResult(err, data));
    });
});

module.exports = tasks_from_teamleader;
