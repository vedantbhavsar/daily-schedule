const mysql = require("mysql"); // returns me object
const express = require("express"); // Express returns me function
const utils = require("./utils");

const tasks_title = express();

const connection = mysql.createConnection({
    host: "localhost",
    user: "sunbeam",
    password: "sunbeam",
    database: "daily_schedule",
});
connection.connect();

var tasks_titledata = [];
tasks_title.get("/", (req, res) => {
    //Fire Query on DB
    connection.query(`select * from tasks_title`, (err, data) => {
        res.send(utils.createResult(err, data));
    });
});

module.exports = tasks_title;
