const mysql = require("mysql"); // returns me object
const express = require("express"); // Express returns me function
const utils = require("./utils");

const manager = express();

const connection = mysql.createConnection({
    host: "localhost",
    user: "sunbeam",
    password: "sunbeam",
    database: "daily_schedule",
});
connection.connect();

var managerdata = [];
manager.get("/", (req, res) => {
    //Fire Query on DB
    connection.query("select * from employee", (err, data) => {
        res.send(utils.createResult(err, data));
    });
});

manager.get("/:manager_id", (req, res) => {
    connection.query(
        `select * from employee where (manager_id = ${req.params.manager_id} and employee_id = ${req.params.manager_id}) or employee_id = ${req.params.manager_id}`,
        (err, data) => {
            res.send(utils.createResult(err, data));
        }
    );
});

manager.post("/", (req, res) => {
    //let manager_id  = req.body.manager_id;
    let first_name = req.body.first_name;
    let last_name = req.body.last_name;
    let password = req.body.password;
    let role = req.body.role;
    let designation = req.body.designation;
    let company_email = req.body.company_email;
    let company_mobile = req.body.company_mobile;
    // let birth_date = req.body.birth_date;
    // let personal_email = req.body.personal_email;
    // let personal_mobile = req.body.personal_mobile;
    let joining_date = req.body.joining_date;
    // let resign_date = req.body.resign_date;

    let query = `insert into manager( first_name, last_name, password, role, designation, company_email, company_mobile,
        joining_date) values
        ('${first_name}', '${last_name}', '${password}','${role}','${designation}', '${company_email}',
        '${company_mobile}', '${joining_date}');`;
    connection.query(query, (err, data) => {
        res.send(utils.createResult(err, data));
    });

    // let query = `insert into manager( first_name, last_name, password, role, designation, company_email, company_mobile,
    //     birth_date, personal_email, personal_mobile, joining_date, resign_date) values
    //     ('${first_name}', '${last_name}', '${password}','${role}','${designation}', '${company_email}',
    //     '${company_mobile}','${birth_date}','${personal_email}','${personal_mobile}', '${joining_date}','${resign_date}');`;
    // connection.query(query, (err, data) => {
    //     res.send(utils.createResult(err, data));
    // });
});

manager.put("/:employee_id", (req, res) => {
    let employee_id = req.params.employee_id;
    let first_name = req.body.first_name;
    let last_name = req.body.last_name;
    let password = req.body.password;
    let role = req.body.role;
    let team_id = req.body.team_id;
    // let company_email = req.body.company_email;
    // let company_mobile = req.body.company_mobile;
    let birth_date = req.body.birth_date;
    let personal_email = req.body.personal_email;
    let personal_mobile = req.body.personal_mobile;
    let address = req.body.address;
    // let joining_date = req.body.joining_date;
    let resign_date = req.body.resign_date;
    let query = `update employee set first_name = '${first_name}', last_name = '${last_name}', password = '${password}',
        role = '${role}', team_id = ${team_id}, resign_date = '${resign_date}', address = '${address}',
        personal_email = '${personal_email}', personal_mobile = '${personal_mobile}' where employee_id = ${employee_id};`;
    connection.query(query, (err, data) => {
        res.send(utils.createResult(err, data));
    });
});
module.exports = manager;
