const mysql = require("mysql"); // returns me object
const express = require("express"); // Express returns me function
const utils = require("./utils");

const tasks_from_manager = express();

const connection = mysql.createConnection({
    host: "localhost",
    user: "sunbeam",
    password: "sunbeam",
    database: "daily_schedule",
});
connection.connect();

var tasks_from_managerdata = [];
tasks_from_manager.get("/", (req, res) => {
    //Fire Query on DB
    connection.query("select * from tasks_from_manager", (err, data) => {
        res.send(utils.createResult(err, data));
    });
});

tasks_from_manager.get("/:manager_task_id", (req, res) => {
    connection.query(
        `select * from tasks_from_manager where manager_task_id = ${req.params.manager_task_id}`,
        (err, data) => {
            res.send(utils.createResult(err, data));
        }
    );
});

tasks_from_manager.get("/manager/:manager_id", (req, res) => {
    connection.query(
        `select * from tasks where assignee_id = ${req.params.manager_id}`,
        (err, data) => {
            res.send(utils.createResult(err, data));
        }
    );
});

tasks_from_manager.post("/:manager_id/:teamleader_id/:title_id", (req, res) => {
    // let manager_task_id  = req.body.manager_task_id;
    let assignee_id = req.params.manager_id;
    let assigned_id = req.params.teamleader_id;
    let title_id = req.params.title_id;
    let team_id = req.body.team_id;
    let task_title = req.body.task_title;
    let task_description = req.body.task_description;
    let start_date = req.body.start_date;
    let end_date = req.body.end_date;
    // let status = req.body.status;

    console.log(assignee_id, assigned_id, team_id, title_id, task_title, task_description, start_date, end_date);

    let query = `insert into tasks(assignee_id, assigned_id, team_id, title_id, task_title, task_description, start_date, end_date) values
        (${assignee_id}, ${assigned_id}, ${team_id}, ${title_id}, '${task_title}', '${task_description}','${start_date}','${end_date}');`;
    connection.query(query, (err, data) => {
        res.send(utils.createResult(err, data));
    });
});

module.exports = tasks_from_manager;
