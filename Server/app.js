const express = require("express"); // Express returns me function
const bodyparser = require("body-parser");
const admin = require("./admin");
const manager = require("./manager");
const employee = require("./employee");
const teams = require("./teams");
const manager_login = require("./manager_login");
const employee_login = require("./employee_login");
const tasks_from_manager = require("./tasks_from_manager");
const tasks_from_teamleader = require("./tasks_from_teamleader");
const tasks_title = require("./tasks_title");

const app = express();
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept"
    );
    next();
});
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));
app.use("/admin", admin);
app.use("/manager", manager);
app.use("/employee", employee);
app.use("/teams", teams);
app.use("/manager_login", manager_login);
app.use("/employee_login", employee_login);
app.use("/tasks_from_manager", tasks_from_manager);
app.use("/tasks_from_teamleader", tasks_from_teamleader);
app.use("/tasks_title", tasks_title);

app.get("/", (req, res) => {
    res.send(`<h1> Welcome To Node</h1>`);
});

app.listen(4500, () => {
    console.log("Server started listening...");
});
