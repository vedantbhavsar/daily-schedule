const mysql = require("mysql"); // returns me object
const express = require("express"); // Express returns me function
const utils = require("./utils");

const admin = express();

const connection = mysql.createConnection({
    host: "localhost",
    user: "sunbeam",
    password: "sunbeam",
    database: "daily_schedule",
});
connection.connect();

admin.get("/manager", (req, res) => {
    connection.query(`select * from employee where role = 'Manager'`, (err, data) => {
        res.send(utils.createResult(err, data));
    });
});

admin.get("/teamleader", (req, res) => {
    connection.query(
        `select * from employee where role = 'Team Leader'`,
        (err, data) => {
            res.send(utils.createResult(err, data));
        }
    );
});

admin.get("/teammember", (req, res) => {
    connection.query(
        `select * from employee where role = 'Team Member'`,
        (err, data) => {
            res.send(utils.createResult(err, data));
        }
    );
});

admin.get("/employees", (req, res) => {
    connection.query(`select * from employee`, (err, data) => {
        res.send(utils.createResult(err, data));
    });
});

admin.get("/team", (req, res) => {
    connection.query(`select * from teams`, (err, data) => {
        res.send(utils.createResult(err, data));
    });
});

admin.get("/taskmanager", (req, res) => {
    connection.query(`select * from tasks`, (err, data) => {
        res.send(utils.createResult(err, data));
    });
});

admin.get("/taskteamleader", (req, res) => {
    connection.query(`select * from tasks`, (err, data) => {
        res.send(utils.createResult(err, data));
    });
});

admin.post("/", (req, res) => {
    let email = req.body.email;
    let password = req.body.password;
    connection.query(
        `select * from admin where admin_email = '${email}' and password = '${password}'`,
        (err, data) => {
            res.send(utils.createResult(err, data));
        }
    );
});

admin.put("/:new_manager/:old_manager", (req, res) => {
    // let manager_id = req.params.manager_id;

    let query = `update employee set  manager_id = '${req.params.new_manager}' where manager_id = ${req.params.old_manager};`;
    connection.query(query, (err, data) => {
        res.send(utils.createResult(err, data));
    });
});

admin.put("/:new_manager/:old_manager", (req, res) => {
    //let manager_id = req.params.manager_id;

    let query = `update team set  manager_id = '${req.params.new_manager}' where manager_id = ${req.params.old_manager};`;
    connection.query(query, (err, data) => {
        res.send(utils.createResult(err, data));
    });
});

admin.put("/:new_manager/:old_manager", (req, res) => {
    // let manager_id = req.params.manager_id;

    let query = `update task_from_manager set  manager_id = '${req.params.new_manager}' where manager_id = ${req.params.old_manager};`;
    connection.query(query, (err, data) => {
        res.send(utils.createResult(err, data));
    });
});

module.exports = admin;
