const mysql = require("mysql"); // returns me object
const express = require("express"); // Express returns me function
const utils = require("./utils");

const teams = express();

const connection = mysql.createConnection({
    host: "localhost",
    user: "sunbeam",
    password: "sunbeam",
    database: "daily_schedule",
});
connection.connect();

var teamsdata = [];
teams.get("/", (req, res) => {
    //Fire Query on DB
    connection.query(`select * from teams`, (err, data) => {
        res.send(utils.createResult(err, data));
    });
});

teams.get("/:team_id", (req, res) => {
    //Fire Query on DB
    connection.query(
        `select * from employee where team_id = ${req.params.team_id}`,
        (err, data) => {
            res.send(utils.createResult(err, data));
        }
    );
});

teams.get("/manager/:manager_id", (req, res) => {
    //Fire Query on DB
    connection.query(
        `select * from teams where manager_id = ${req.params.manager_id}`,
        (err, data) => {
            res.send(utils.createResult(err, data));
        }
    );
});

teams.get(`/:manager_id/:team_id`, (req, res) => {
    //Fire Query on DB
    connection.query(
        `select * from employee where manager_id = ${req.params.manager_id} and team_id = ${req.params.team_id}`,
        (err, data) => {
            res.send(utils.createResult(err, data));
        }
    );
});

teams.post("/:manager_id", (req, res) => {
    let manager_id = req.params.manager_id;
    let team_name = req.body.team_name;
    console.log(manager_id + team_name);
    let query = `insert into teams (team_name, manager_id) values('${team_name}', ${manager_id})`;
    connection.query(query, (err, data) => {
        res.send(utils.createResult(err, data));
    });
});

module.exports = teams;
